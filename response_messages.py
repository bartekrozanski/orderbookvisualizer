import enum
class ResponseMessage(object):
    @enum.unique
    class ResponseCodes(enum.Enum):
        SUCCESS = 0
        INVALID_TICKER = 1
        NON_EXISTING_TICKER = 2
        BOT_WARN = 3
        ADAPTER_ERROR = 4


    @staticmethod
    def create_success_message(payload):
        return {
            "code":ResponseMessage.ResponseCodes.SUCCESS.value,
            "data": payload
            }

    @staticmethod
    def create_invalid_ticker_message():
        return {
            "code":ResponseMessage.ResponseCodes.INVALID_TICKER.value,
            "data": None
            }

    @staticmethod
    def create_non_existing_ticker_message():
        return {
            "code":ResponseMessage.ResponseCodes.NON_EXISTING_TICKER.value,
            "data": None
            }
    @staticmethod 
    def create_bot_warn_message():
        return {
            "code":ResponseMessage.ResponseCodes.BOT_WARN.value,
            "data": None
            }

    @staticmethod 
    def create_adapter_error_message():
        return {
            "code":ResponseMessage.ResponseCodes.ADAPTER_ERROR.value,
            "data": None
            }