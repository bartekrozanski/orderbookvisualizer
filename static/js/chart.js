var data = null;
var title = null;
var display_mode = "accumulated";

function update_chart() {
  remove_children(document.getElementById("container"));
  var max_order_count = -1;
  var buy_orders = clone2dArray(data["buy_orders"]);
  var sell_orders = clone2dArray(data["sell_orders"]);

  if(display_mode == "accumulated"){
    accumulate_volume(buy_orders);
    accumulate_volume(sell_orders);
  }
  for (var order of buy_orders.concat(sell_orders)){
    if(order[2] > max_order_count){
      max_order_count = order[2];
    }
  }
  for(var order of buy_orders){
    order.push(anychart.color.darken("#00cc00", (order[2])/(max_order_count*1.5)));
  }
  for(var order of sell_orders){
    order.push(anychart.color.darken("#cc0000", (order[2])/(max_order_count*1.5)));
  }
  var datasetBuy = anychart.data.set(buy_orders);
  var dataSeriesBuy =  datasetBuy.mapAs({x: 0, value: 1, orders_count: 2, fill: 3, stroke: 3});
  var datasetSell = anychart.data.set(sell_orders);
  var dataSeriesSell =  datasetSell.mapAs({x: 0, value: 1, orders_count: 2, fill: 3, stroke: 3});
  
  if(display_mode == "accumulated"){
    accumulate_volume(buy_orders);
    accumulate_volume(sell_orders);
    var chart = anychart.area();
    var buySeries = chart.stepArea(dataSeriesBuy);
    var sellSeries = chart.stepArea(dataSeriesSell);
  }
  else if(display_mode == "normal"){
    var chart = anychart.stick();
    var buySeries = chart.stick(dataSeriesBuy);
    var sellSeries = chart.stick(dataSeriesSell);
  }
    chart.xScale(anychart.scales.linear());
    var price_deviation = 0.3;

    let interval_start_index = bisect(buy_orders, 0, buy_orders.length - 1,
      buy_orders[0][0] * (1-price_deviation), x => x[0], (x,y) => x>y);
    let interval_end_index = bisect(sell_orders, 0, sell_orders.length - 1,
      sell_orders[0][0] * (1+price_deviation), x => x[0], (x,y) => x<y);
  
    var interval_start = buy_orders[interval_start_index][0];
    var interval_end = sell_orders[interval_end_index][0];

    chart.xScale().minimum(buy_orders[buy_orders.length-1][0]);
    chart.xScale().maximum(sell_orders[sell_orders.length-1][0]);
    var ticksArray = buy_orders.concat(sell_orders).map(x=>x[0]);
    chart.xScale().ticks().set(ticksArray);
    chart.xScroller(true)

    chart.xZoom().setToValues(interval_start, interval_end);
    chart.title(`Ticker: ${title}. Data from ${data['date']}`);
    chart.xAxis().title("Price");
    chart.yAxis().title("Volume");

    buySeries.tooltip().title("Buy orders");
    buySeries.tooltip().format("Price: {%categoryName} \nOrders: {%orders_count} \nTotal volume: {%value}");
    sellSeries.tooltip().title("Sell orders");
    sellSeries.tooltip().format("Price: {%categoryName} \nOrders: {%orders_count} \nTotal volume: {%value}");
    chart.container("container");
    chart.barGroupsPadding(0);
    chart.barsPadding(-1);
    chart.draw();
}

function change_display_mode(){
  var button = document.getElementById("display_mode_button");
  if(display_mode == "accumulated"){
    console.debug("Changing display mode to stick.");
    display_mode = "normal";
    button.innerHTML = "Change to accumulated view"
  }
  else if(display_mode == "normal"){
    console.debug("Changing display mode to accumulated.");
    display_mode = "accumulated";
    button.innerHTML = "Change to stick view";
  }
  if(data){
    update_chart();
  }
}

function get_data() {
  var xhttp = new XMLHttpRequest();
  input_field = document.getElementsByName("ticker_input")[0];
  if(input_field.value == "") return;
  var ticker = input_field.value.toUpperCase();
  var endpoint = "";
  if(ticker.startsWith("TEST")){
    ticker = ticker.slice(4);
    console.log('Will target test endpoint');
    endpoint = `/api/orders/test/${ticker}`;
  }else{
    console.log('Will target real data endpoint');
    endpoint = `/api/orders/${ticker}`
  }
  console.log(ticker);
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.body.classList.remove("loading");
        var response = JSON.parse(this.responseText);
        console.debug(`Response code: ${response.code}`);
        switch (response.code) {
          case 0:
            setDisplayMode("wrongTickerError", "none");
            data = response.data;
            title = ticker;
            update_chart();
            console.debug(response);
            break;
          case 1:
              document.getElementById("wrongTickerError").innerHTML = `This does not look like a correct ticker`;
              setDisplayMode("wrongTickerError", "block");
            break;
          case 2:
                document.getElementById("wrongTickerError").innerHTML = `Could not load data for this ticker. Check if it is listed on Warsaw Stock Exchange.`;
                setDisplayMode("wrongTickerError", "block");
            break;
          case 3:
                document.getElementById("wrongTickerError").innerHTML = `Data source is not responding. It probably has moved this website on its blacklist :C`;
                setDisplayMode("wrongTickerError", "block");
                break;
          case 4:
                document.getElementById("wrongTickerError").innerHTML = `Server could not parse data from data source.`;
                setDisplayMode("wrongTickerError", "block");
                break;
          default:
                document.getElementById("wrongTickerError").innerHTML = `Unknown response code.`;
                setDisplayMode("wrongTickerError", "block");
                break;
        }
      }
  };
  xhttp.ontimeout = function(e){
    document.getElementById("wrongTickerError").innerHTML = `Timeout. Probably the data source is down.`;
    setDisplayMode("wrongTickerError", "block");
    document.body.classList.remove("loading");
  }
  console.log(`Getting data for from ${ticker}`);
  xhttp.open("GET", endpoint, true);
  xhttp.timeout = 10 * 1000;
  document.body.classList.add("loading");
  xhttp.send();
}