function remove_children(element){
    while (element.lastElementChild){
        element.removeChild(element.lastElementChild);
    }
}
  
function setDisplayMode(element_id, value) {
var x = document.getElementById(element_id);
x.style.display = value;
}
  
function getNormalizedColor(a,b, val){  
return (255 - Math.ceil((((val-a)/b) * 200))).toString(16);
}

function clone2dArray(arr){
    var newArray = []
    for (var i = 0; i < arr.length ; ++i){
        newArray[i] = arr[i].slice();
    }
    return newArray;
}

function accumulate_volume(orders){
    var acc = 0
    for (var i = 0; i < orders.length ; ++i){
      orders[i][1] += acc;
      acc = orders[i][1];
    }
    return orders
}

function bisect(arr, a, b, val, extractor, comparer){
    if(b-a <= 1){
        if(Math.abs(extractor(arr[b]) - val) < Math.abs(extractor(arr[a]) - val)){
            return b;
        }
        else return a;
    } 
    var c = Math.round((b-a)/2) + a;
    if(comparer(val, extractor(arr[c]))){
        return bisect(arr,a,c,val,extractor, comparer);
    }
    else{
        return bisect(arr,c,b,val,extractor, comparer);
    }
}
