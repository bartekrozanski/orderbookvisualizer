import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] [%(name)s] %(message)s",
    handlers=[
        # logging.FileHandler("debug.log"),
        logging.StreamHandler()
    ]
)
logger = logging.getLogger(__name__)


from flask import Flask, url_for, jsonify
from .scrapper.scrapper import Scrapper
from .scrapper.cacher import Cacher
from .scrapper.data_provider import TestDataProvider, GraGieldowaDataProvider
from .scrapper.data_adapter import DataAdapter
from .scrapper.session_provider import SessionProvider
from .response_messages import ResponseMessage



caching_time_in_seconds = 5 * 60 # 5 minutes
logger.debug("Starting Flask app")
app = Flask(__name__)
cacher = Cacher('cached_pages', caching_time_in_seconds)
test_cacher = Cacher('cached_test_pages', caching_time_in_seconds)
session_provider = SessionProvider()
data_provider = GraGieldowaDataProvider(cacher, session_provider)
test_data_provider = TestDataProvider(test_cacher, 'test_data')
scrapper = Scrapper()
data_adapter = DataAdapter()

invalid_ticker_response = {"code": 1, "data":None}

def get_data(data_provider, scrapper, data_adapter, ticker):
    data = data_provider.get_data_for_ticker(ticker)
    status_code, scrapped_data = scrapper.scrap(data)
    if status_code != Scrapper.StatusCodes.SUCCESS:
        return status_code
    code, buy_orders = data_adapter.transform_data(scrapped_data["buy_orders"])
    if code == DataAdapter.StatusCodes.PARSE_ERROR:
        return code
    code, sell_orders = data_adapter.transform_data(scrapped_data["sell_orders"])
    if code == DataAdapter.StatusCodes.PARSE_ERROR:
        return code
        
    scrapped_data["buy_orders"] = buy_orders
    scrapped_data["sell_orders"] = sell_orders
    return scrapped_data

def is_possibly_a_valid_ticker(ticker):
    if len(ticker) > 5 :
        return False
    if any(map(lambda x : not str.isalpha(x), ticker)):
        return False
    return True

@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('html/start_page.html')


def get_data_for_ticker(ticker, data_provider):
    data = None
    if not is_possibly_a_valid_ticker(ticker):
        return jsonify(ResponseMessage.create_invalid_ticker_message())
    ticker = ticker.upper()
    data = get_data(data_provider, scrapper, data_adapter, ticker)
    if data == Scrapper.StatusCodes.INVALID_TICKER:
        return jsonify(ResponseMessage.create_non_existing_ticker_message())
    if data == Scrapper.StatusCodes.BOT_WARN:
        return jsonify(ResponseMessage.create_bot_warn_message())
    if data == DataAdapter.StatusCodes.PARSE_ERROR:
        return jsonify(ResponseMessage.create_adapter_error_message())
    return jsonify(ResponseMessage.create_success_message(data))


@app.route('/api/orders/test/<string:ticker>')
def api_test_orders(ticker):
    return get_data_for_ticker(ticker, test_data_provider)
    
@app.route('/api/orders/<string:ticker>', methods=['GET'])
def api_orders(ticker):
    return get_data_for_ticker(ticker, data_provider)
