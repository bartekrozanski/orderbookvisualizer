import requests
from bs4 import BeautifulSoup
import logging
import enum
logger = logging.getLogger(__name__)

class Scrapper(object):
    class StatusCodes(enum.Enum):
        SUCCESS = enum.auto()
        INVALID_TICKER = enum.auto()
        BOT_WARN = enum.auto()

    def __init__(self):
        pass

    def table_to_row_data(self, table):
        rows = table.find_all('tr')
        rows = rows[2:-1] #drop headers and summary rows
        result = [[cell.text for cell in html_row.find_all('td')] for html_row in rows]
        return result

    def scrap(self, data):
        soup = BeautifulSoup(data, 'html.parser')
        if(self.contains_error(soup)):
            return Scrapper.StatusCodes.INVALID_TICKER, None
        if(self.contains_bot_warn(soup)):
            return Scrapper.StatusCodes.BOT_WARN, None
        tables = soup.find_all('table')
        buy_orders_table = tables[0]
        sell_orders_table = tables[2]
        buy_orders_data = self.table_to_row_data(buy_orders_table)
        sell_orders_data = self.table_to_row_data(sell_orders_table)
        date = self.scrap_date(soup)

        return Scrapper.StatusCodes.SUCCESS, {'date': date, 'buy_orders': buy_orders_data, 'sell_orders': sell_orders_data}

    def contains_error(self, soup):
        return len(soup.findAll("div", {"class" : "error"})) == 1

    def contains_bot_warn(self, soup):
        return len(soup.findAll("div", {"class" : "warn"})) == 1

    def scrap_date(self, soup):
        return soup.findAll("div", {"class":"profile"})[0].findAll("p")[1].text[23:-1]
