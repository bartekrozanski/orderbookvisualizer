import requests
import logging

logger = logging.getLogger(__name__)

class SessionProvider(object):
    login = "JaneKowalskiJan"
    password = "NoweHaslo"
    url = "https://gragieldowa.pl" 

    def __init__(self):
        self.session = None

    def get_session(self):
        if not self.session:
            self.session = self.create_session()
        return self.session

    def create_session(self):
        logger.debug("Creating new session")
        payload = {'uLogin': SessionProvider.login, 'uHaslo': SessionProvider.password}
        session = requests.Session()
        session.get(SessionProvider.url)
        session.post(SessionProvider.url, data=payload)
        return session

