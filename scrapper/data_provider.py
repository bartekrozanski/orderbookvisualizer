import os
import logging
import requests

logger = logging.getLogger(__name__)

class BaseDataProvider(object):
    def __init__(self, cacher):
        self.cacher = cacher

    def get_data_for_ticker(self, ticker):
        logger.info(f"Getting data for {ticker}")
        data = self.cacher.retrieve_data(ticker)
        if not data:
            logger.info("Data was not present in cache. Loading data...")
            data = self.load_data(ticker)
            logger.info("Data loaded")
            self.cacher.cache_data(data, ticker)
            logger.info("Data cached")
            return data
        else:
            logger.info("Returning data from cache")
            return data

class TestDataProvider(BaseDataProvider):
    def __init__(self, cacher, path_to_test_data):
        super().__init__(cacher)
        self.path = path_to_test_data

    def load_data(self, ticker):
        file_to_serve = os.path.join(self.path, f"{ticker}.html")
        if not os.path.isfile(file_to_serve):
            logger.warning(f'File {file_to_serve} does not exist')
            with open(os.path.join(self.path, f"FOO_not_existing.html")) as file:
                return file.read()
        logger.info(f"Reading data from {file_to_serve}")
        with open(file_to_serve, 'r') as file:
            return file.read()


class GraGieldowaDataProvider(BaseDataProvider):
    @staticmethod
    def get_url(ticker):
        return f'https://gragieldowa.pl/spolka_arkusz_zl/spolka/{ticker}'

    def __init__(self, cacher, session_provider):
        self.session_provider = session_provider
        self.cacher = cacher

    def load_data(self, ticker):
        target_url = GraGieldowaDataProvider.get_url(ticker)
        logger.info(f"Downloading data from {target_url}...")
        response = self.session_provider.get_session().get(target_url)
        return response.text


