import glob
from datetime import datetime, timedelta
import os
import logging

logger = logging.getLogger(__name__)

class Cacher(object):
    date_format = "%d-%m-%Y-%H-%M-%S"

    @staticmethod
    def get_filename(name):
        timestamp = datetime.now().strftime(Cacher.date_format) 
        return f'{name}_{timestamp}'

    def __init__(self, working_directory, caching_time):
        self.working_directory = working_directory
        self.caching_time = timedelta(seconds=caching_time) 

    def cache_data(self, data, name):
        path = os.path.join(self.working_directory, Cacher.get_filename(name))
        with open(path, 'w') as file:
            file.write(data)
            logger.info(f"Caching data to {path}")
        

    def get_currently_cached_files(self):
        return [
                file for file in os.listdir(self.working_directory)
                if os.path.isfile(os.path.join(self.working_directory, file))
                ]

    def retrieve_data(self, name):
        logger.info(f"Retrieving data for {name}")
        currently_cached_files = self.get_currently_cached_files()
        currently_cached_files = list(filter(lambda x: x.startswith(f'{name}_'), currently_cached_files))
        if (len(currently_cached_files) > 1):
            logger.warning("There are more than one cached file with the same name. Invalidating cache.")
            [self.remove_file(file) for file in currently_cached_files]
            return None

        if(len(currently_cached_files) == 1):
            if(self.check_if_exceeds_caching_time(currently_cached_files[0])):
                logger.info("Cached file was too old. Removing it...")
                self.remove_file(currently_cached_files[0])
                return None
            else:
                with open(os.path.join(self.working_directory, currently_cached_files[0]), 'r') as file:
                    return file.read()
                

    def remove_file(self, filename):
        file_path = os.path.join(self.working_directory, filename)
        os.remove(file_path)

    def check_if_exceeds_caching_time(self, file_path):
        filename = os.path.basename(file_path)
        timestamp = filename[filename.find('_')+1:]
        file_date = datetime.strptime(timestamp, Cacher.date_format)
        return datetime.now() - file_date > self.caching_time
    
