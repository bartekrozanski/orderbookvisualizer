import logging
import enum
logger = logging.getLogger(__name__)

class DataAdapter(object):
    class StatusCodes(enum.Enum):
        SUCCESS = enum.auto()
        PARSE_ERROR = enum.auto()

    def transform_data(self, data, dropped_first_row = False):
        def transform_row(row):
            return [
                float(row[0].replace(',','.').replace(' ', '')),
                float(row[1].replace(',','.').replace(' ', '')),
                int(row[3].replace(',','.').replace(' ', '')),
            ]
        try:
            output = [transform_row(row) for row in data] 
            return DataAdapter.StatusCodes.SUCCESS, output
        except ValueError as e:
            logger.error("Unknown error in data adapter")
            logger.error(e)
            if not dropped_first_row:
                logger.error('Trying parsing without first row...')
                code, output = self.transform_data(data[1:], True) 
                return code, output
            else:
                return DataAdapter.StatusCodes.PARSE_ERROR, None

