# What is it?
Web application using Flask which allows visualizing order book of Warsaw Stock Exchange (WSE).\
For one month it was hosted on Azure (azure-deployment-ready version is tagged) with 50 views daily on average.

It scrapes other website for required data (WSE's order book) and displays it in a graphical manner instead of a dull table.\
Thanks to that, you can easily spot support and resistance levels or how high are supply and demand of given stock.

# What does it look like?
<img src="screenshots/screen1.png" width="600" height="300" />
<img src="screenshots/screen2.png" width="600" height="300" />
<img src="screenshots/screen3.png" width="600" height="300" />

# Setup
1. Clone this repo
2. Step into downloaded directory
2. Create virtual environment with `python3 -m venv .venv`
2. Activate venv with `source .venv/bin/activate` 
2. Install requirements with `pip3 install -requirements.txt`

# Running
Run `run.sh` with venv activated. Browse to `localhost:5000`.
